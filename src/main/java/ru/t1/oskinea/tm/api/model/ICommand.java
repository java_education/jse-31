package ru.t1.oskinea.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.enumerated.Role;

public interface ICommand {

    void execute();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    @NotNull
    String getName();

    @Nullable
    Role[] getRoles();

}
