package ru.t1.oskinea.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project changeProjectStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project changeProjectStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @NotNull String description);

}
