package ru.t1.oskinea.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.Domain;
import ru.t1.oskinea.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from backup file.";

    @NotNull
    public static final String NAME = "backup-load";

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(bytes, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
